# test42 - documented learning though testing


## run 

Just clone and do the `mvn clean test`. Thats actually all this project does. There are test that must fail. The are marked with the `@Tag("must_fail")`. To exclude them from testing use `mvn test -Dgroups=\!must_fail`. To only execute these tests call `mvn test -Dgroups=must_fail`.

Same for `@Tag("needs_network")`. To exclude all test which must fail **or** need network do
```
mvn clean test -Dgroups=\!must_fail\&\!needs_network
```
[This](https://junit.org/junit5/docs/current/user-guide/#running-tests-tag-expressions) explains *Tag Expressions* which can easyíly be tried in the intellj run configuration. 

## going further

Its somehow not easy to find documentation how to apply tags in the mvn command line. Beside `groups` I found also ['excludeTags'](https://stackoverflow.com/q/52606191) and `-Dtest`. I guess the first was just stupid and the second refers to single tests. 

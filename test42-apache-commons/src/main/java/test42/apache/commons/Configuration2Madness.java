package test42.apache.commons;

import net.kafujo.base.UncheckedException;
import net.kafujo.units.KafuNumber;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.SystemConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;

import java.io.File;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * I am a big fan of the apache commons projects, but configurations2 is a disaster. It writes back to the
 * objects passed in as a configuration source. This can be especially harmful when using
 * {@link SystemConfiguration} as demonstrated here.
 *
 * I did not write this as normal unit tests cause this code
 * modifies the System properties which might cause other tests to fail.
 */
public class Configuration2Madness {

    /**
     * Creates a configuration which first reads the System Properties.
     *
     * @param fbkPropertiesFile fall back file, if the property is not in system property
     * @return config ready to use.
     */
    public static Configuration createConfiguration(File fbkPropertiesFile) {
        SystemConfiguration priorityOne = new SystemConfiguration(); // if it comes with -D its the law

        // create and add the backup based on a properties files
        try {
            PropertiesConfiguration fbk = new Configurations().properties(fbkPropertiesFile);
            priorityOne.append(fbk);
        } catch (ConfigurationException failed) {
            throw new UncheckedException("invalid config file", failed);
        }

        priorityOne.setThrowExceptionOnMissing(true);
        return priorityOne;
    }


    /**
     * If there is a System property when the conf is created, configurations2 will do weird things to it.
     */
    public static void sysProbAvailable() {
        //  assume some parameter were set using -D (I tried it with the same result)
        System.setProperty("CONF2_MADNESS_PORT", "443");

        // check, if it worked
        assertThat(System.getProperty("CONF2_MADNESS_PORT")).isEqualTo("443");

        // now we build a config and read the value as int
        Configuration conf = createConfiguration(new File("conf2madness.properties"));
        assertThat(conf.getInt("CONF2_MADNESS_PORT")).isEqualTo(443);

        // but now, there is no system property anymore:
        assertThat(System.getProperty("CONF2_MADNESS_PORT")).isNull();

        // Its still there, but its not a String anymore, but a list of Strings
        assertThat(List.of("443", "80")).isEqualTo(System.getProperties().get("CONF2_MADNESS_PORT"));

        // no matter how one would explain it, its IMHO totally insane. You can get such weird
        // runtime errors and even exceptions when you try to reset the Property

        try {
            assertThat(System.setProperty("CONF2_MADNESS_PORT", "23"));
            throw new AssertionError("Wont reach this point");
        } catch (ClassCastException wellDone) {
            System.out.println("WELL DONE:" + wellDone);
        }
    }

    /**
     * If there is NO System property, configurations2 will create it, which one might not always want
     */
    static void sysProbNotAvailable() {

        // nobody set it, so its null
        assertThat(System.getProperty("CONF2_MADNESS_PORT")).isNull();

        Configuration conf = createConfiguration(new File("conf2madness.properties"));
        assertThat(conf.getInt("CONF2_MADNESS_PORT")).isEqualTo(80); // from the file

        // MAGIC: now we have a new System property!!
        assertThat(System.getProperty("CONF2_MADNESS_PORT")).isEqualTo("80");

        System.setProperty("CONF2_MADNESS_PORT", "888");
        assertThat(System.getProperty("CONF2_MADNESS_PORT")).isEqualTo("888");

        assertThat(conf.getInt("CONF2_MADNESS_PORT")).isEqualTo(888); // from system properties

        // I dont care, how the story goes on
    }

    public static void main(String[] args) {

        // you cannot call both in one go cause either "destroy" the system properties
        if (KafuNumber.randomBoolean()) {
            sysProbAvailable();
        } else {
            sysProbNotAvailable();
        }
    }

}

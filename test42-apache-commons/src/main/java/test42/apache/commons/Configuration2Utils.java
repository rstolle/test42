package test42.apache.commons;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.MapConfiguration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;

import java.io.File;
import java.util.Properties;

/**
 * Two utility method to be used in test and to illustrate how nice it good be, if there weren't the
 * {@link Configuration2Madness madness}.
 */
public final class Configuration2Utils {

    /**
     * Creates a {@link Configuration} which uses the properties of {@code props} and if not found there, the ones
     * of {@code backupProperties}. If a key is not in both NoSuchElementException will be thrown.
     *
     * @param props         main source for properties
     * @param fbkProperties backup
     * @return conf
     * @throws ConfigurationException on any error
     */
    public static Configuration createChainedConf(final Properties props, final File fbkProperties) throws ConfigurationException {

        final var primaryConf = new MapConfiguration(props);
        primaryConf.setThrowExceptionOnMissing(true);

        // create and add the backup based on a properties files
        PropertiesConfiguration backup = new Configurations().properties(fbkProperties);
        primaryConf.append(backup);

        return primaryConf;
    }

    /**
     * Creates a {@link Configuration} which uses the properties of {@code prio1} and if not found there, the ones
     * in {@code fbk}. If a key is not in both NoSuchElementException will be thrown.
     *
     * @param prio1 main source for properties
     * @param fbk   fallback properties, if not found in prio1
     * @return conf
     */
    public static Configuration createChainedConf(final Properties prio1, final Properties fbk) {
        final var conf = new MapConfiguration(prio1);
        conf.append(new MapConfiguration(fbk));
        conf.setThrowExceptionOnMissing(true);

        return conf;
    }
}

package test42.apacon.lang3;

import org.junit.jupiter.api.Test;

import static org.apache.commons.lang3.StringUtils.*;
import static org.junit.jupiter.api.Assertions.*;

class StringUtils_Test {


    @Test
    void nulls() {
        assertNull(leftPad(null, 2));
        assertNull(abbreviate(null, 2));
        assertNull(left(null, -1));
    }

    @Test
    void leftPad01() {
        assertEquals("Hallo", leftPad("Hallo", 2));
        assertEquals("Hallo", leftPad("Hallo", 5));
        assertEquals("  Hallo", leftPad("Hallo", 7));
        assertEquals("     Hallo", leftPad("Hallo", 10));
    }

    @Test
    void rightPad01() {
        assertEquals("  Hallo", rightPad("  Hallo", 2));
        assertEquals("Hallo", rightPad("Hallo", 5));
        assertEquals("Hallo  ", rightPad("Hallo", 7));
        assertEquals("Hallo     ", rightPad("Hallo", 10));
    }

    @Test
    void abbr01() {
        assertThrows(IllegalArgumentException.class, () -> abbreviate("Hallo", 2));
        assertEquals("Hallo", abbreviate("Hallo", 5));
        assertEquals("Ha...", abbreviate("Hallo World", 5));
    }

    @Test
    void left01() {
        assertEquals("", left("a", -1));
        assertEquals("", left("a", 0));
        assertEquals("a", left("a", 1));
        assertEquals("a", left("a", 2));

        assertEquals("a", left("ab", 1));
        assertEquals("ab", left("ab", 2));
        assertEquals("ab", left("abcd", 2));
        assertEquals("", left("", 2));
    }

}

package test42.apacon.conf2;

import net.kafujo.config.TypedProperties;
import org.apache.commons.configuration2.MapConfiguration;
import org.apache.commons.configuration2.SystemConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This test demonstrated how configuration2 read and writes through {@link Properties} passed in as a configuration
 * foundation. This can cause serious runtime problems, when the original properties will be used elsewhere. E.g.
 * {@link SystemConfiguration} does this stuff with the {@link System#getProperties() system properties}, which cause
 * dangerous side effects illustrated in {@link test42.apache.commons.Configuration2Madness}. No normal test for this,
 * cause the side effects could destroy other tests which one can see checking out commit
 * 6b72d07842b8a8a5531a3262c2886f38265c890d and run mvn test on Windows 7.
 */
class BrokenChaining_Test {

    private static final Properties PROPS_ONE = new Properties();
    private static final Properties PROPS_TWO = TypedProperties.loadResource("types.properties");

    @BeforeAll
    static void beforeAll() {
        PROPS_ONE.setProperty("INTEGER", "23");
        PROPS_ONE.setProperty("ONLY_SYS", "true");
    }

    @Test
    void chaining1_2() throws ConfigurationException {

        final var config = new MapConfiguration(PROPS_ONE);
        config.append(new MapConfiguration(PROPS_TWO));
        config.setThrowExceptionOnMissing(true);

        assertEquals(23, config.getInt("INTEGER"));                 // ONE
        assertEquals("Hello World", config.getString("HELLO"));     // TWO
        assertThrows(NoSuchElementException.class, () -> config.getString("INTHJAGHKJNLEGER"));

        // so far, so good. Now we change the underlying properties:
        PROPS_ONE.setProperty("HELLO", "No fallback anymore!");  // changing this has effect on config:
        assertEquals("No fallback anymore!", config.getString("HELLO"));     // now: ONE

        // I dont like it, but its still ok. NOT OK ist the following:

        PROPS_ONE.remove("HELLO");    // Remove HELLO from ONE ...
        assertNull(PROPS_ONE.getProperty("HELLO"));
        assertEquals("Hello World", PROPS_TWO.getProperty("HELLO"));  // ... but its still in TWO ...

        // ... but not in the config anymore:
        assertThrows(NoSuchElementException.class, () -> config.getString("HELLO"));  // THIS IS INSANE!!

        // Its getting worse. Look what happened to "INTEGER":
        assertNull(PROPS_ONE.getProperty("INTEGER"));  // null, cause its no String anymore ...
        assertEquals(List.of("23", "42"), PROPS_ONE.get("INTEGER")); // .. but a List !!

        // now imagine PROPS_ONE was System.getProperties!

    }
}


# what I dont like

This lib tries to cover every possible situation. It IMHO jus to much. Common, simple 
use cases need a lot knowledge and useless boilerplate code. 

 - Its takes a while to figure out what are necessary steps.
 - Runtime dependencies to beanutils, even on the simplest code. Why is it not included?
 
# inconsistencies 

 - why exists `getInteger()` only with a default option?
 
# The System Properties Bug

Using (System) Properties as a source can [cause serious trouble](BrokenChaining_Test.java). I consider this behavior 
as too dangerous ever use this library. 
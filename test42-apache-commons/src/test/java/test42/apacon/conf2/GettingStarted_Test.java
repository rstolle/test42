package test42.apacon.conf2;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class GettingStarted_Test {

    /**
     * A minmal example scratched out of the "Quick start guide"
     * (https://commons.apache.org/proper/commons-configuration/userguide/quick_start.html)
     *
     * @throws ConfigurationException on conf problems
     */
    @Test
    void minimal_from_file() throws ConfigurationException {
        final Configurations configs = new Configurations();
        Configuration config = configs.properties(new File("types.properties"));

        assertEquals(42, config.getInteger("INTEGER", 0));
        assertEquals(42, config.getInt("INTEGER"));
        assertEquals(3.1415, config.getDouble("DOUBLE"));

        assertEquals("Hello World", config.getString("HELLO"));

        // strips the text in opposite to Properties ..
        assertEquals("Hello World", config.getString("TEXT_NOT_STRIPPED"));

        // default values or not there
        assertEquals(23, config.getInteger("INTHJAGHKJNLEGER", 23));
        assertNull(config.getString("INTHJAGHKJNLEGER")); // Bad! Exception would be better!
    }

    @Test
    void minimal_with_exception() throws ConfigurationException {
        final Configurations configs = new Configurations();

        // Configuration config  doesn't have setThrowExceptionOnMissing()
        PropertiesConfiguration config = configs.properties(new File("types.properties"));
        config.setThrowExceptionOnMissing(true);
        assertThrows(NoSuchElementException.class, () -> config.getString("INTHJAGHKJNLEGER"));
    }
}

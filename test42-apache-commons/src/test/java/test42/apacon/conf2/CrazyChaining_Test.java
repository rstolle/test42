package test42.apacon.conf2;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.configuration2.ex.ConversionException;
import org.junit.jupiter.api.Test;
import test42.apache.commons.Configuration2Utils;

import java.io.File;
import java.util.NoSuchElementException;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CrazyChaining_Test {

    private final static File FALLBACK = new File("types.properties");

    @Test
    void useOwnProperties() throws ConfigurationException {
        final Properties props = new Properties();
        props.setProperty("INTEGER", "4711");

        Configuration conf = Configuration2Utils.createChainedConf(props, FALLBACK);
        assertEquals(4711, conf.getInt("INTEGER"));     // comes from props
        assertEquals("Hello World", conf.getString("HELLO")); // not in props, but in Backup
        assertEquals("missing", conf.getString("N/A", "missing")); // not in both
        assertThrows(NoSuchElementException.class, () -> conf.getString("N/A"));
        assertThrows(ConversionException.class, () -> conf.getInt("HELLO"));

        props.setProperty("INTEGER", "54321");  // change a value in backing map ...
        assertEquals("54321", props.getProperty("INTEGER"));  // ... of course. But it will modify ...
        assertEquals(54321, conf.getInt("INTEGER"));     // ... conf as well!!
        // I dont like this behavior, but its still better than in the next test.
    }
}

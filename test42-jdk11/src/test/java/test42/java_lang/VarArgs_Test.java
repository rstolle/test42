package test42.java_lang;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Shows some pitfalls when using varargs parameter
 */
class VarArgs_Test {

    /**
     * There could be no argument or even worse, a NPE
     *
     * @param numbers numbers
     * @return the number of numbers
     */
    private long unsafe(int... numbers) {
        return Arrays.stream(numbers).count();
    }

    /**
     * Covers at least the NPE. Zero args are still possible
     *
     * @param numbers numbers
     * @return number of numbers
     */
    private long safe(int... numbers) {

        if (numbers == null || numbers.length == 0) {
            return 0;
        }
        return Arrays.stream(numbers).count();
    }

    /**
     * Covers the NPE and ensures, there is at least one value.
     *
     * @param first the first number - must be there
     * @param more  additional numbers
     * @return number of numbers
     */
    private long best(int first, int... more) {
        if (more == null || more.length == 0) {
            return 1;
        }
        return Arrays.stream(more).count() + 1;
    }


    @Test
    void varargs_unsafe() {
        assertThrows(NullPointerException.class, () -> unsafe(null));
        assertEquals(0, unsafe());
        assertEquals(1, unsafe(1));
        assertEquals(2, unsafe(1, 2));
    }

    @Test
    void arrays_unsafe() {
        assertThrows(NullPointerException.class, () -> unsafe((int[]) null));
        assertEquals(0, unsafe(new int[]{}));
        assertEquals(1, unsafe(new int[]{1}));
        assertEquals(2, unsafe(new int[]{1, 2}));
    }

    @Test
    void varargs_safe() {
        assertEquals(0, safe(null));
        assertEquals(0, safe());
        assertEquals(1, safe(1));
        assertEquals(2, safe(1, 2));
    }

    @Test
    void arrays_safe() {
        assertEquals(0, safe((int[]) null));
        assertEquals(0, safe(new int[]{}));
        assertEquals(1, safe(new int[]{1}));
        assertEquals(2, safe(new int[]{1, 2}));
    }

    @Test
    void varargs_atLeastOne() {
        assertEquals(1, best(1));
        assertEquals(1, best(1, null));
        assertEquals(2, best(1, 2));
    }

    @Test
    void array_atLeastOne() {
        assertEquals(1, best(1, (int[]) null));
        assertEquals(1, best(1, new int[]{}));
        assertEquals(2, best(1, new int[]{2}));
        assertEquals(3, best(1, new int[]{2, 3}));
    }
}

package test42.junit5;

import org.junit.jupiter.api.*;

import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Four tests with execution order by method name (MethodOrderer.Alphanumeric)")
@TestMethodOrder(MethodOrderer.Alphanumeric.class)
class OrderAlphanumeric_Test {

    private static final List<Integer> list = new LinkedList<>();

    @Test
    void one() {
        assertEquals(1, list.size());
        list.add(2);
    }

    @Test
    void two() {
        assertEquals(3, list.size());
        assertTrue(list.contains(1));
        assertTrue(list.contains(2));
        assertTrue(list.contains(3));
        list.add(4);
    }

    @Test
    void three() {
        assertEquals(2, list.size());
        list.add(3);
    }

    @Test
    void four() {
        assertTrue(list.isEmpty());
        list.add(1);

    }
}
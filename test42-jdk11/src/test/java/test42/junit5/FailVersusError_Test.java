package test42.junit5;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.fail;

class FailVersusError_Test {

    @Tag("must_fail")
    @Test
    void failByFailMethod () {
        fail("fuack"); // just throws a AssertionFailedError, which derives from AssertionError
    }

    @Tag("must_fail")
    @Test
    void failByHand () {
        throw new AssertionError("This test failed badly");
    }

    @Tag("must_fail")
    @Test
    void causeError () {
        throw new IllegalStateException("This test caused an error");
    }
}

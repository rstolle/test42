package test42.junit5;

import org.junit.jupiter.api.*;

@DisplayName("Five tests with random execution order (MethodOrderer.Random)")
@TestMethodOrder(MethodOrderer.Random.class)
class OrderRandom_Test {

    @BeforeAll
    static void beforeAll() {
        System.out.print(" RANDOM ORDER:      ");
    }

    @AfterAll
    static void afterAll() {
        System.out.println("  DONE");
    }

    @DisplayName("1")
    @Test
    void one() {
        System.out.print(" 1 ");
    }

    @DisplayName("2")
    @Test
    void two() {
        System.out.print(" 2 ");
    }

    @DisplayName("3")
    @Test
    void three() {
        System.out.print(" 3 ");
    }

    @DisplayName("4")
    @Test
    void four() {
        System.out.print(" 4 ");
    }

    @DisplayName("5")
    @Test
    void five() {
        System.out.print(" 5 ");
    }

}
package test42.junit5;

import org.junit.jupiter.api.*;

import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Four tests with execution order by MethodOrderer.OrderAnnotation")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class OrderByAnnotation_Test {

    private static final List<Integer> list = new LinkedList<>();
    private static int ctorCount = 0;

    /**
     * Illustrates, that an object is created for each test method.
     */
    OrderByAnnotation_Test() {
        ctorCount++;
    }

    @Order(1)
    @Test
    void one() {
        assertEquals(1, ctorCount);
        assertTrue(list.isEmpty());
        list.add(1);
    }

    @Order(2)
    @Test
    void two() {
        assertEquals(2, ctorCount);
        assertEquals(1, list.size());
        list.add(2);
    }

    @Order(3)
    @Test
    void three() {
        assertEquals(3, ctorCount);
        assertEquals(2, list.size());
        list.add(3);
    }

    @Order(4)
    @Test
    void four() {
        assertEquals(4, ctorCount);
        assertEquals(3, list.size());
        assertTrue(list.contains(1));
        assertTrue(list.contains(2));
        assertTrue(list.contains(3));
        list.add(4);
    }

}
package test42.jdk.collections;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Shows different ways how to duplicate Lists. Most of this is valid for Sets and Maps as well.
 */
class DuplicateLists_Test {

    private List<Integer> oldList;

    @BeforeEach
    void beforeEach() {
        oldList = new ArrayList<>();
        oldList.add(1);
        oldList.add(2);
        oldList.add(3);
    }

    @Test
    void naive() {
        assertEquals(3, oldList.size());
        // this is what would happen, if you do a return list from within your objects
        var newList = oldList;

        newList.add(4);
        assertTrue(newList.contains(4));
        assertTrue(oldList.contains(4));
    }

    @Test
    void betterButMaybeNotWhatYouWant() {
        var newList = Collections.unmodifiableList(oldList);

        // cannot add to newList anymore
        assertThrows(UnsupportedOperationException.class, () -> newList.add(4));

        // but to the original list:
        oldList.add(4);

        assertTrue(newList.contains(4)); // newList is still backed by strings
    }

    @Test
    void betterButVerbose() {
        var newList = Collections.unmodifiableList(new ArrayList<>(oldList));

        // cannot add to newList anymore
        assertThrows(UnsupportedOperationException.class, () -> newList.add(4));

        // but to the original list:
        oldList.add(4);

        assertFalse(newList.contains(4)); // newList ist totally detached from strings
    }

    @Test
    void betterJdk11() {
        var newList = List.copyOf(oldList);

        // cannot add to newList anymore
        assertThrows(UnsupportedOperationException.class, () -> newList.add(4));

        // but to the original list:
        oldList.add(4);

        assertFalse(newList.contains(4)); // newList ist totally detached from strings
    }

}

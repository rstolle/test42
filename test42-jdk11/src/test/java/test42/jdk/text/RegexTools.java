package test42.jdk.text;

import java.util.List;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RegexTools {

    static void checkPattern(final String patternStr, final List<String> matches, final List<String> noMatches) {
        Pattern pattern = Pattern.compile (patternStr);
        for (String match : matches) {
            assertTrue(pattern.matcher(match).matches(), "'" + match + "' should match '" + pattern + "'");
        }
        for (String noMatch : noMatches) {
            assertFalse(pattern.matcher(noMatch).matches(), "'" + noMatch + "' should NOT match  '" + pattern + "'");
        }
    }
}

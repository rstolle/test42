package test42.jdk.text;


import org.junit.jupiter.api.Test;

import java.util.List;

import static test42.jdk.text.RegexTools.checkPattern;

class RegexSimple_Test {

    @Test
    void exactlyTheSame() {
        checkPattern("Only you",
                List.of("Only you"),
                List.of("only you", " Only you"));
    }

    @Test
    void matchesAnyCharacter() {
        checkPattern(".nly you",
                List.of("Only you",     // matches
                        "only you",
                        " nly you"),

                List.of("  ly you",     // no match
                        " Only you"));
    }

    @Test
    void matchesOo() {
        checkPattern("[Oo]nly you",
                List.of("Only you",
                        "only you"),

                List.of(" nly you",
                        "  ly you",
                        " Only you"));
    }

    @Test
    void endlessOo() {
        checkPattern("[Oo]*nly you",
                List.of("nly you",
                        "Only you",
                        "only you",
                        "Oonly you",
                        "OOOOOOOOOOOOOOOoooooooooooooooonly you",
                        "OoOoOoOoOoOonly you"),

                List.of(" OoOoOoOoOoOonly you",
                        " nly you",
                        "  ly you",
                        " Only you"));
    }

    @Test
    void endlessAnyCharacters() {
        checkPattern(".*nly you",
                List.of("Only you",
                        "only you",
                        "Oonly you",
                        "OOOOOOOOOOOOOOOoooooooooooooooonly you",
                        "OoOoOoOoOoOonly you",
                        " OoOoOoOoOoOonly you",
                        " nly you",
                        " Only you",
                        "asklföasjfaösjdcfasklhgaösqnly you"),

                List.of("  ly you"));
    }

    @Test
    void limitOo() {
        checkPattern("[Oo]{2}nly you",
                List.of("Oonly you",
                        "oOnly you",
                        "OOnly you",
                        "oonly you"),

                List.of("Only you",
                        "OoOoOoOoOoOonly you",
                        " OoOoOoOoOoOonly you",
                        " nly you",
                        " Only you",
                        "asklföasjfaösjdcfasklhgaösqnly you",
                        "  ly you"));
    }

    @Test
    void limitOo2() {
        checkPattern("[Oo]{2}nly you",
                List.of("Oonly you",
                        "oOnly you",
                        "OOnly you",
                        "oonly you"),

                List.of("Only you",
                        "OoOoOoOoOoOonly you",
                        " OoOoOoOoOoOonly you",
                        " nly you",
                        " Only you",
                        "asklföasjfaösjdcfasklhgaösqnly you",
                        "  ly you"));
    }
}

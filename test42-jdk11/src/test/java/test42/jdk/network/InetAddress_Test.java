package test42.jdk.network;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class InetAddress_Test {

    @Tag("needs_network")
    @Test
    void checkAddress() throws UnknownHostException {
        InetAddress addr = InetAddress.getByName("www.heise.de");
        assertEquals("www.heise.de", addr.getHostName());
        System.out.println(" *** www.heise.de: " + addr.getHostAddress());
    }
}

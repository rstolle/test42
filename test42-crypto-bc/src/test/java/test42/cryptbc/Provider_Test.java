package test42.cryptbc;

import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.Test;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.util.Base64;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


class Provider_Test {


    @Test
    void add() {

        var before = Security.getAlgorithms("MessageDigest").size();

        Security.addProvider(new BouncyCastleProvider());

        var after = Security.getAlgorithms("MessageDigest").size();

        assertTrue(before < after);
    }

}
